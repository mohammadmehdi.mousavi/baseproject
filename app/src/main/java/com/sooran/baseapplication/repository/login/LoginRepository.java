package com.sooran.baseapplication.repository.login;

import com.sooran.baseapplication.model.User;

import io.reactivex.Flowable;

public interface LoginRepository {

    Flowable<User> login(String grant_type,
                         String auth_method,
                         String username,
                         String password);
}
