package com.sooran.baseapplication.di.auth;

import androidx.lifecycle.ViewModel;

import com.sooran.baseapplication.di.ViewModelKey;
import com.sooran.baseapplication.ui.auth.AuthViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class AuthViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(AuthViewModel.class)
    public abstract ViewModel bindAuthViewModel(AuthViewModel authViewModel);
}
