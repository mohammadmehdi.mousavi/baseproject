package com.sooran.baseapplication.data.dao;

import com.sooran.baseapplication.model.User;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public interface DaoUser {

    public Flowable<User> getUser(String grant_type, String auth_method, String username, String password);
}
