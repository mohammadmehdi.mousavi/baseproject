package com.sooran.baseapplication.repository.login;

import com.sooran.baseapplication.data.dao.DaoUser;
import com.sooran.baseapplication.di.auth.AuthScope;
import com.sooran.baseapplication.model.User;

import javax.inject.Inject;

import io.reactivex.Flowable;


@AuthScope
public class LoginRepositoryImp implements LoginRepository {

    private LoginRepositoryFactory loginRepositoryFactory;

    @Inject
    public LoginRepositoryImp(LoginRepositoryFactory loginRepositoryFactory){
        this.loginRepositoryFactory=loginRepositoryFactory;
    }


    @Override
    public Flowable<User> login(String grant_type,
                                String auth_method,
                                String username,
                                String password) {
        DaoUser daoUser;
        daoUser=loginRepositoryFactory.getdao(grant_type,auth_method,username,password);
        if(daoUser!=null){

            return daoUser.getUser(grant_type,auth_method,username,password);

        }
        User ErrorUser = new User();
        ErrorUser.setAccessToken("1");
        return Flowable.just(ErrorUser);
    }

}
