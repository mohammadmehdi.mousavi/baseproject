package com.sooran.baseapplication.repository.login;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.test.core.app.ApplicationProvider;

import com.sooran.baseapplication.data.dao.db.UserDao;
import com.sooran.baseapplication.data.dao.net.UserDaoNet;
import com.sooran.baseapplication.model.User;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;

import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class LoginRepositoryImpTest {

    private LoginRepository loginRepository;
    @Mock
    private LoginRepositoryFactory loginRepositoryFactory;
    @Mock
    private UserDao userDao;
    @Mock
    private UserDaoNet userDaoNet;
    @Mock
    private SharedPreferences sharedPreferences;
    private TestSubscriber testSubscriber;


    @Test
    public void loginNetSuccess() {


    }

    @Test
    public void loginLocalSuccess() {

        User user=new User("sfjlsdfjsklfjskfjsdfklsfsdfsfdf","reza");
/*
        userDao=new UserDao(sharedPreferences);
*/

        /*Mockito.when(sharedPreferences.getString(Mockito.anyString(), Mockito.anyString()).length() > 25)
                .thenReturn(true);*/

        Mockito.when(userDao.getUser(Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any()))
                .thenReturn(Flowable.just(user));

        Mockito.when(loginRepositoryFactory.getdao(Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any()))
                .thenReturn(userDao);

        loginRepository.login("","","","").subscribe(testSubscriber);
        verify(userDao).getUser(Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any());

     //   Mockito.verify(loginRepository,Mockito.times(1)).login(Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any());
     //  Mockito.verifyNoMoreInteractions(userDao);
        testSubscriber.assertComplete();
        testSubscriber.assertResult(user);


    }

    @Before
    public void setUp() throws Exception {
/*
        MockitoAnnotations.initMocks(this);
*/
        loginRepository= new LoginRepositoryImp(loginRepositoryFactory);
        testSubscriber=new TestSubscriber();
    }
}