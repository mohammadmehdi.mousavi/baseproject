package com.sooran.baseapplication.di;

import android.app.Application;
import android.content.Context;

import com.sooran.baseapplication.BaseApplication;
import com.sooran.baseapplication.di.auth.AuthViewModelModule;
import com.sooran.baseapplication.ui.auth.AuthActivity;
import com.sooran.baseapplication.di.auth.AuthModule;
import com.sooran.baseapplication.di.auth.AuthScope;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

    @AuthScope
    @ContributesAndroidInjector(modules = {
            AuthModule.class,
            AuthViewModelModule.class
    })
    abstract AuthActivity contributeAuthActivity();

    @Binds
    abstract Context bindsContext(Application application);

}
