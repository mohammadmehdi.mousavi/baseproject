package com.sooran.baseapplication.di;

import android.app.Application;

import com.sooran.baseapplication.BaseApplication;
import com.sooran.baseapplication.SessionManager;
import com.sooran.baseapplication.data.dao.db.UserDao;
import com.sooran.baseapplication.repository.login.LoginRepository;
import com.sooran.baseapplication.repository.login.LoginRepositoryFactory;
import com.sooran.baseapplication.repository.login.LoginRepositoryImp;
import com.sooran.baseapplication.services.login.LoginService;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ViewModelFactoryModule.class,
        AppModule.class,
        ActivityBuilderModule.class,
})
public interface AppComponent extends AndroidInjector<BaseApplication> {

    SessionManager sessionManager();
    @Component.Builder
    interface Builder{
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }
}
