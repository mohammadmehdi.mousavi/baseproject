package com.sooran.baseapplication.ui.auth;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.sooran.baseapplication.R;
import com.sooran.baseapplication.databinding.ActivityAuthBinding;
import com.sooran.baseapplication.model.User;
import com.sooran.baseapplication.viewmodels.ViewModelProviderFactory;
import javax.inject.Inject;
import javax.inject.Named;

import dagger.android.support.DaggerAppCompatActivity;
import retrofit2.Retrofit;

public class AuthActivity extends DaggerAppCompatActivity {

    private AuthViewModel viewModel;
    @Inject
    ViewModelProviderFactory factory;
    ActivityAuthBinding binding;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel= ViewModelProviders.of(this,factory).get(AuthViewModel.class);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_auth);
        editor=sharedPreferences.edit();
        viewModel.observeAuthState().observe(this, new Observer<AuthResource<User>>() {
            @Override
            public void onChanged(AuthResource<User> userAuthResource) {

                if(userAuthResource!=null){

                    switch (userAuthResource.status){

                        case LOADING:{
                            showProgressBar(true);
                            break;
                        }
                        case AUTHENTICATED:{
                            Log.d("login","login success    "+ userAuthResource.data.getAccessToken());
                            showProgressBar(false);
                            break;
                        }
                        case ERROR:{
                            showProgressBar(false);
                            editor.remove("Token").apply();
                            editor.remove("GivenName").apply();
                            Toast.makeText(AuthActivity.this, userAuthResource.data.getError_message(),Toast.LENGTH_SHORT).show();

                            break;
                        }
                        case NOT_AUTHENTICATED:{
                            showProgressBar(false);

                            break;
                        }
                    }
                }
            }


        });
        viewModel.authenticateWithId(null,null,null,null);
    /*    viewModel.getError().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean){
                    Toast.makeText(getApplicationContext(),viewModel.errorText,Toast.LENGTH_LONG).show();
                }
            }
        });*/


        binding.signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);*/
                if (binding.user.getText().length()==0 || binding.pass.getText().length()==0){
                    Toast.makeText(getApplicationContext(),"لطفا در ورود اطلاعات دقت کنید." ,
                            Toast.LENGTH_SHORT).show();
                }
                else {
                    viewModel.authenticateWithId("password","password",
                            binding.user.getText().toString(),
                            binding.pass.getText().toString());
                }

            }
        });

    }

    private void showProgressBar(boolean isVisible){
        if(isVisible){
            binding.progressBar.setVisibility(View.VISIBLE);
        }
        else {
            binding.progressBar.setVisibility(View.INVISIBLE);
        }

    }
}
