package com.sooran.baseapplication.data.dao.db;

import android.content.SharedPreferences;

import com.sooran.baseapplication.data.dao.DaoUser;
import com.sooran.baseapplication.model.User;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Singleton
public class UserDao implements DaoUser {

    private SharedPreferences sharedPreferences;
    @Inject
    public UserDao(SharedPreferences sharedPreferences){

        this.sharedPreferences = sharedPreferences;
    }



    @Override
    public Flowable<User> getUser(String grant_type, String auth_method, String username, String password){
        if(sharedPreferences.getString("Token", "").length() > 25){

            return Flowable.just(new User(sharedPreferences.getString("Token", ""),sharedPreferences.getString("GivenName", "")));

        }
        return null;
    }
}
