package com.sooran.baseapplication.repository.login;

import android.content.SharedPreferences;

import com.sooran.baseapplication.data.dao.DaoUser;
import com.sooran.baseapplication.data.dao.db.UserDao;
import com.sooran.baseapplication.data.dao.net.UserDaoNet;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LoginRepositoryFactory {

    private SharedPreferences sharedPreferences;
    private UserDao userDao;
    private UserDaoNet userDaoNet;

    @Inject
    public LoginRepositoryFactory(SharedPreferences sharedPreferences, UserDao userDao, UserDaoNet userDaoNet){

        this.sharedPreferences = sharedPreferences;
        this.userDao = userDao;
        this.userDaoNet = userDaoNet;
    }


    public DaoUser getdao(String grant_type,
                          String auth_method,
                          String username,
                          String password){

        if(sharedPreferences.getString("Token", "").length() > 30 && grant_type==null){
            return userDao;
        }
        else {
            if(grant_type==null){
                return null;
            }
            else {
                return userDaoNet;
            }
        }
    }

}
