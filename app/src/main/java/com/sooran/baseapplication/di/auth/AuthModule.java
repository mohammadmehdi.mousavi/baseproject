package com.sooran.baseapplication.di.auth;

import android.content.SharedPreferences;

import com.sooran.baseapplication.data.dao.db.UserDao;
import com.sooran.baseapplication.data.dao.net.UserDaoNet;
import com.sooran.baseapplication.network.AuthApi;
import com.sooran.baseapplication.repository.login.LoginRepository;
import com.sooran.baseapplication.repository.login.LoginRepositoryFactory;
import com.sooran.baseapplication.repository.login.LoginRepositoryImp;
import com.sooran.baseapplication.services.login.LoginService;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class AuthModule {

    @Provides
    @AuthScope
    static AuthApi provideAuthApi (@Named("auth") Retrofit retrofit){
        return retrofit.create(AuthApi.class);
    }
    @AuthScope
    @Provides
    static LoginService provideloginService(AuthApi authApi){
        return new LoginService(authApi);
    }

    @AuthScope
    @Provides
    static LoginRepositoryFactory provideloginRepositoryFactory(SharedPreferences sharedPreferences, UserDao userDao, UserDaoNet userDaoNet){
        return new LoginRepositoryFactory(sharedPreferences,userDao,userDaoNet);
    }
    @AuthScope
    @Provides
    static UserDaoNet provideuserDaoNet(LoginService loginService){
        return new UserDaoNet(loginService);
    }

    @AuthScope
    @Provides
    static UserDao provideuserDao(SharedPreferences sharedPreferences){
        return new UserDao(sharedPreferences);
    }

    @AuthScope
    @Provides
    static LoginRepository provideLoginRepository(LoginRepositoryFactory loginRepositoryFactory){
        return new LoginRepositoryImp(loginRepositoryFactory);
    }

}
