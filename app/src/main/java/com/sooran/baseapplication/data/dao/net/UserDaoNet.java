package com.sooran.baseapplication.data.dao.net;

import com.sooran.baseapplication.data.dao.DaoUser;
import com.sooran.baseapplication.model.User;
import com.sooran.baseapplication.services.login.LoginService;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Singleton public class UserDaoNet implements DaoUser {

    private LoginService loginService;


    @Inject
    public UserDaoNet(LoginService loginService) {

        this.loginService = loginService;
    }

    @Override
    public Flowable<User> getUser(String grant_type, String auth_method, String username, String password) {

        return loginService.getlogin(grant_type,auth_method,username,password);
    }
}
