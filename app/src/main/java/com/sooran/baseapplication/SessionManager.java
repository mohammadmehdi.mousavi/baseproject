package com.sooran.baseapplication;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;


import com.sooran.baseapplication.model.User;
import com.sooran.baseapplication.ui.auth.AuthResource;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SessionManager {

    private static final String TAG="SessionManager";

    private MediatorLiveData<AuthResource<User>> cachedUser= new MediatorLiveData<>();

    @Inject

    public SessionManager() {
    }

    public void setUserLogin(AuthResource<User> user){
        cachedUser.setValue(user);
    }

    public void authenticateWithId(LiveData<AuthResource<User>> source) {

        if(cachedUser!= null){
            cachedUser.setValue(AuthResource.loading((User)null));
            cachedUser.addSource(source, new Observer<AuthResource<User>>() {
                @Override
                public void onChanged(AuthResource<User> userAuthResource) {
                    cachedUser.setValue(userAuthResource);
                    cachedUser.removeSource(source);
                }
            });
        }
        else {

        }

    }

    public void logOut(){
        Log.d(TAG,"     Logging out ...");
        cachedUser.setValue(AuthResource.logout());
    }

    public LiveData<AuthResource<User>> getAuthUser(){
        return cachedUser;
    }
}
