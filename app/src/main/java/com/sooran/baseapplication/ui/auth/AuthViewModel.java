package com.sooran.baseapplication.ui.auth;

import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.ViewModel;

import com.sooran.baseapplication.SessionManager;
import com.sooran.baseapplication.data.dao.db.UserDao;
import com.sooran.baseapplication.model.User;
import com.sooran.baseapplication.network.AuthApi;
import com.sooran.baseapplication.repository.login.LoginRepository;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class AuthViewModel extends ViewModel {

    private static final String TAG="AuthViewModel";
    private final AuthApi authApi;
    private SessionManager sessionManager;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private UserDao userDao;
    private final LoginRepository loginRepository;

    @Inject
    public AuthViewModel(AuthApi authApi, SessionManager sessionManager, SharedPreferences sharedPreferences, UserDao userDao, LoginRepository loginRepository) {
        this.authApi = authApi;
        this.sessionManager = sessionManager;
        this.sharedPreferences = sharedPreferences;
        editor=sharedPreferences.edit();
        this.userDao = userDao;
        this.loginRepository = loginRepository;
    }

    public LiveData<AuthResource<User>> observeAuthState(){
        return sessionManager.getAuthUser();
    }

    public void authenticateWithId(String grant_type,
                                   String auth_method,
                                   String username,
                                   String password){
        Log.d(TAG, "attempting to login");

        sessionManager.authenticateWithId(queryUserId(grant_type,
                auth_method,
                username,
                password));

    }

    private LiveData<AuthResource<User>> queryUserId(String grant_type,
                                                     String auth_method,
                                                     String username,
                                                     String password) {



            return LiveDataReactiveStreams.fromPublisher(
                    loginRepository.login(grant_type,
                            auth_method,
                            username,
                            password)
                            .onErrorReturn(new Function<Throwable, User>() {
                                @Override
                                public User apply(Throwable throwable) throws Exception {
                                    User errorUser = new User();
                                    errorUser.setAccessToken("1");
                                    errorUser.setError_message(throwable.getMessage());

                                    return errorUser;
                                }
                            })
                            .map(new Function<User, AuthResource<User>>() {
                                @Override
                                public AuthResource<User> apply(User user) throws Exception {
                                    if (user.getAccessToken().equals("1")) {
                                        return AuthResource.error("Could not authenticate", user);
                                    }
                                    editor.putString("Token", user.getAccessToken());
                                    editor.putString("GivenName", user.getGivenName());
                                    editor.apply();
                                    return AuthResource.authenticated(user);
                                }
                            })
                            .subscribeOn(Schedulers.io()));

    }

}
