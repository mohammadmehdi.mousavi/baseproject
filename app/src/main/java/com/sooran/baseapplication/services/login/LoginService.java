package com.sooran.baseapplication.services.login;

import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;

import com.sooran.baseapplication.di.auth.AuthScope;
import com.sooran.baseapplication.model.User;
import com.sooran.baseapplication.network.AuthApi;
import com.sooran.baseapplication.ui.auth.AuthResource;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

@AuthScope
public class LoginService {

    AuthApi authApi;
    @Inject
    public LoginService(AuthApi authApi){
        this.authApi=authApi;
    }

    public Flowable<User> getlogin(String grant_type,
                              String auth_method,
                              String username,
                              String password ){


        return authApi.getToken(grant_type,
                auth_method,
                username,
                password);

    }
}
