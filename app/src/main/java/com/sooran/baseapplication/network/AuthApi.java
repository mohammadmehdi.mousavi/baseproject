package com.sooran.baseapplication.network;

import com.sooran.baseapplication.model.User;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AuthApi {

    @FormUrlEncoded
    @POST("token")
    Flowable<User> getToken(@Field("grant_type") String grant_type,
                            @Field("auth_method") String auth_method,
                            @Field("username") String username,
                            @Field("password") String password);

}
